/// Shelby Taylor
/// CSE 002 HW 05
/// October 9, 2018

import java.util.Scanner;
import java.lang.Math;

public class Hw05 {
  public static void main (String [] arg){
  
  int onePairCounter = 0; // initializing the counter for one pair in a hand
  int threeOfAKindCounter = 0; // initializing the counter for three of a kind in a hand
  int twoPairCounter = 0; // initializing the counter for two pairs in a hand
  int fourOfAKindCounter = 0; // initializing the counter for four of a kind in a hand 
  int match = 0; // initializing the counter for a match within the hand 
  int counter = 0; // initializing overall counter
  int nothing = 0; // initializing when there are no matches at all in the hand 

  
  Scanner myScanner = new Scanner(System.in);
  System.out.print("Enter the number of hands you wish to generate:  "); // prompting the user the decide the numbers of hands she wishes to play
  int decision = myScanner.nextInt();
    while (counter < (decision +1)) {
  
  int cardOne = (int)(Math.random()*(52))+1; // declaring the first card in the hand (randomly generating the card)
  int cardTwo = (int)(Math.random()*(52))+1;  // declaring the second card in the hand (randomly generating the card)
  int cardThree = (int)(Math.random()*(52))+1;  // declaring the third card in the hand (randomly generating the card)
  int cardFour = (int)(Math.random()*(52))+1;  // declaring the fourth card in the hand (randomly generating the card)
  int cardFive = (int)(Math.random()*(52))+1;  // declaring the fifth card in the hand (randomly generating the card)
    
     
  // ensures no cards repeat
  while (cardOne == cardTwo ) { 
    cardOne = (int)(Math.random()*(52))+1;
  }

  
  // ensures no cards repeat
  while (cardOne == cardThree) {
    cardOne = (int)(Math.random()*(52))+1;
  }

  
 // ensures no cards repeat
  while (cardOne == cardFour) {
    cardOne = (int)(Math.random()*(52))+1;
  }

  
 // ensures no cards repeat
  while (cardOne == cardFive) {
    cardOne = (int)(Math.random()*(52))+1;
  }

  
 // ensures no cards repeat 
  while (cardTwo == cardThree) {
    cardTwo = (int)(Math.random()*(52))+1;
  }

  
 // ensures no cards repeat  
  while (cardTwo == cardFour) {
    cardTwo = (int)(Math.random()*(52))+1;
  }

  
 // ensures no cards repeat    
  while (cardTwo == cardFive) {
    cardTwo = (int)(Math.random()*(52))+1;
  }

   
 // ensures no cards repeat  
  while (cardThree == cardFour) {
    cardThree = (int)(Math.random()*(52))+1;
  }

 // ensures no cards repeat
  while (cardThree == cardFive) {
    cardThree = (int)(Math.random()*(52))+1;
  }

  
  // ensures no cards repeat
  while (cardFour == cardFive) {
    cardFour = (int)(Math.random()*(52))+1;
  }
  

// ensuring that cards will only print in numbers that are available in a real deck 
      cardOne = cardOne % 13;
      cardTwo = cardTwo % 13;
      cardThree = cardThree % 13;
      cardFour = cardFour % 13;
      cardFive = cardFive % 13;
    
    // is there is a four of a kind, count
    if (cardOne == cardTwo && cardOne == cardThree && cardOne == cardFour ||
        cardOne == cardTwo && cardOne == cardThree && cardOne == cardFive ||
        cardOne == cardThree && cardOne == cardFour && cardOne == cardFive ||
        cardOne == cardTwo && cardOne == cardFour && cardOne == cardFive ||
        cardTwo == cardThree && cardTwo == cardFour && cardTwo == cardFive) {
      fourOfAKindCounter++;
    }
      // if there is a three of a kind, count
      else if (cardOne == cardTwo && cardOne == cardThree ||
               cardOne == cardTwo && cardOne == cardFour ||
               cardOne == cardTwo && cardOne == cardFive ||
               cardOne == cardThree && cardOne == cardFour ||
               cardOne == cardThree && cardOne == cardFive ||
               cardOne == cardFour && cardOne == cardFive||
               cardTwo == cardThree && cardTwo == cardFour ||
               cardTwo == cardFour && cardTwo == cardFive ||
               cardTwo == cardThree && cardTwo == cardFive || 
               cardThree == cardFour && cardThree == cardFive) {
        threeOfAKindCounter++;
      }
      // count pairs (will later help with determining one pair or two pairs)
        else {
          if (cardOne == cardTwo) {
            match++;
          }
          if (cardOne == cardThree){
            match++;
          }
          if (cardOne == cardFour) {
            match++;
          }
          if(cardOne == cardFive) {
            match++;
          }
          if(cardTwo == cardThree) {
            match++;
          }
          if(cardTwo == cardFour) {
            match++;
          }
          if(cardTwo == cardFive) {
            match++;
          }
          if (cardThree == cardFour) {
            match++;
          }
          if (cardThree == cardFive){
            match++;
          }
          if (cardFour == cardFive) {
            match++;
          }
        }
      // if there is one match, then it will count for one pair
      if (match == 1) {
        onePairCounter++;
      }
      // if there is two matches, then it will count for two pairs
      else if(match == 2) {
        twoPairCounter++;
      }
      else {
        nothing++;
      }
    
    counter ++;
    }

    double onePairProb = (double) onePairCounter / decision; // determining the probability of one pair in the number of hands you wish to play
    double twoPairProb = (double) twoPairCounter / decision; // determining the probability of two pairs in the number of hands you wish to play
    double fourOfAKindProb = (double) fourOfAKindCounter / decision; // determining the probability of having four of a kind in the number of hands you  wish to play
    double threeOfAKindProb = (double) threeOfAKindCounter / decision; // determining the probability of having three of a kind in the number of hands you wish to play
                      
    System.out.printf("The probability of getting one pair is: %.3f\n", onePairProb); // printing the probability of having one pair in the number of hands you wish to play
    System.out.printf("The probaility of getting two pairs is: %.3f\n", twoPairProb); // printing the probability of having two pairs in the number of hands you wish to play
    System.out.printf("The probaility of getting three of a kind is: %.3f\n", threeOfAKindProb); // printing the probability of having three of a kind in the number of hands you wish to play
    System.out.printf("The probability of getting four of a kind is: %.3f\n", fourOfAKindProb); // printing the probability of having four of a kind in the number of hands you wish to play
      
  
  }
                      
}
  
  