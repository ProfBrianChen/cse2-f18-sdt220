/// Shelby Taylor
/// CSE 002 HW06
/// October 21, 2018


import java.util.Scanner;
import java.lang.Math;

public class EncryptedX {
  public static void main (String [] arg){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("Enter a number between 1 and 100:  "); // prompting the user to enter a number between 1 and 100
    
    int numberInput = myScanner.nextInt(); // the number the user enters above
    
    if ( numberInput > 100 || numberInput < 1) { // if the user inputs a number that is not valid
      System.out.print("Error.  Try again");
      numberInput = myScanner.nextInt();
    }
    else { // if the user inputs a number that is valid 
    for (int i = 0; i < numberInput; i++) { // counter for number of rows that would be printed
      System.out.println(""); // moves down to next line
      for (int j = 0; j < numberInput; j++) { 
        // if-statement within for loop for for printing spaces and * of encrypted X 
        if ( i == j || j == (numberInput - i - 1) ) { // determining when to print the space in the line to show the encrypted X
          System.out.print(" ");
        }
        else { 
          System.out.print("*"); // printing the * to show pattern
        }
      }
    }
    }
  }
}