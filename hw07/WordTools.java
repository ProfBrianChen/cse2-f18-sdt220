/// Shelby Taylor
/// CSE 002 Homework 7
/// October 30, 2018

import java.util.Scanner;

public class WordTools {
        
      public static String sampleText() { 
        Scanner myScanner = new Scanner(System.in);
        
        
        System.out.print("Type your sample text: "); //prompts user to type in their sample text
        String textSample = myScanner.nextLine();
        
        System.out.print(" ");
        System.out.println("Your input is: " + textSample); // telling user what input is
        return textSample;
      }
      
      
      public static char printMenu() {
        System.out.println(" c - number of non-whitespace characters."); // counts number of non whitespace characters
        System.out.println(" w - number of words."); // counts number of words
        System.out.println(" f - find text."); // finds text
        System.out.println(" r - replace all exclamation marks (!)."); // replaces !
        System.out.println(" s - shorten spaces."); // shortens spaces
        System.out.println(" q - quit."); // will keep prompting user until user types q to print
        
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Type the letter for the option you want: "); // prompts user to choose a letter for the ones listed above
        
        String decision = myScanner.nextLine();
        char decisionChar = 'x'; // initializes decision
        
        if (decision.length() != 1) {
          decisionChar = 'x'; // the decision of x is now no longer valid, so the user will be prompted to decide on another character
        }
        else {
          decisionChar = decision.charAt(0); // character = letter 
        }
        return decisionChar; 
      }
      
      public static int getNumOfNonWSCharacters(String text) {
        int upperLimit = text.length() - 1; // initializing the upper limit
        int counter = 0; // initialzing counter
        
        for ( int i = 0; i <= upperLimit; i++) { // counter keeps going until it hits upper limit
          char test = text.charAt(i);
          if (!Character.isWhitespace(test)) {
            counter = counter + 1;
          }
        }
        return counter;
      }
      
      
      public static int getNumOfWords(String text) {
        int upperLimit = text.length() - 1; // initializing upper limit
        int counter = 0; // initializing counter
        
        for (int i = 0; i <= upperLimit; i++) {
          char test = text.charAt(i);
          if (Character.isWhitespace(test)) {
            char nextTest = text.charAt(i - 1);
            if (!Character.isWhitespace(nextTest)) {
              counter = counter + 1;
            }
          }
        }
        counter = counter + 1;
        return counter; // return counter
      }
      
      public static int findText(String find, String text) {
        int counter = 0; // initializing counter
        
        if (text.contains(find)) {
          int length = text.length(); // determines length of text
          String text1 = text.replace(find, ""); // replaces the inputs with nothing
          int length1 = text1.length(); // determines the length once all of the inputs are taken out
          counter = (length - length1) / (find.length());  // determines the amount of times find input is used
        }
        return counter; // return counter
      }
      
      public static String replaceExclamation(String text) {
        String textEdit = text.replace("!", "."); // replaces double space with single space
        return textEdit; // return text with no !
      }
      
      public static String shortenSpaces(String text) {
        String textEditNext = text.replace(" ", " "); // replaces double with with single space 
        return textEditNext;  // return text with single spaces rather than double spaces
      }
    
        
  public static void main (String [] args) {
    Scanner myScanner = new Scanner(System.in); 
      String text = sampleText(); 
      int counter = 0; // initialize the counter
      
      while (counter == 0) {
        char character = printMenu();
        switch (character){
          case 'q': // quit
            counter = 1;
            break;
          case 'c': // # non white-space characters
            int varC = getNumOfNonWSCharacters(text); 
            System.out.println("The number of non-white characters is: " + varC); 
            break;
          case 'w': // # words in sample text
            int varW = getNumOfWords(text);
            System.out.println("The number of words is: " + varW);
            break;
          case 'f': // find a specific phrase
            System.out.println("Which word / phrase would you like to find?");
            String find = myScanner.nextLine();
            int varF = findText(find, text);
            System.out.println("\"" + find + "\"" + " instances: " + varF);
            break;
          case 'r': // runs the exclamation replacement method
            String period = replaceExclamation(text);
            System.out.println("Edited: " + period);
            break;
          case 's':
            String spaces = shortenSpaces(text); // runs method that will shorten number of spaces
            System.out.println("Edited: " + spaces); 
            break;
          default:
            System.out.print("Your input in not valid.  Please try again.");
            break;
        }
      }
  }
  }
  