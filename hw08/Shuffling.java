/// Shelby Taylor
/// Homework 08 CSE 002
/// November 14, 2018

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Shuffling{ 
public static void main(String[] args) { 
  Scanner scan = new Scanner(System.in); //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"}; // c - clubs, h - hearts, s - spades, d - diamonds
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; // possible number on card
  String[] cards = new String[52]; 
  String[] hand = new String[5]; 
 
  // declaring integers
  int testVar = 1; 
  int numberCards = 0;
  int again = 1;
  int index = 51;
  
  while (testVar < 2) {
    System.out.print("How many cards would you like in your hand? (choose between 0 and 52): "); // prompting the user to decide how many cards she would like in her hand
    boolean userInput = scan.hasNextInt(); // making sure the user enter an integer
    
    if (userInput) { // if statement for number of cards user decides
      numberCards = scan.nextInt();
      if (numberCards >= 1 && numberCards <= 52) { // checking that the user input is valid, within the range of number of cards in a check
        testVar++; 
      }
    }
    
    else { // user input a number that is not within range
      scan.next();
      System.out.println("Try Again.  The number you entered is not within the possible range."); // printing that the number the user input was not within range
    }
  }
    
    for (int i = 0; i < 52; i++) {
      cards[i] = rankNames[i%13] + suitNames[i/13]; 
      System.out.print(cards[i] + " ");
    }
    
    System.out.println();
    printArray(cards, numberCards);
    shuffle(cards);
    printArray(cards, numberCards);
    
    while (again == 1) {
      hand = getHand(cards,index,numberCards);    
      printArray(hand, numberCards);
      index = index - numberCards;
      System.out.println("Enter the number 1, if you want to draw another hand: "); // prompting the user the decide if she wants another hand
      again = scan.nextInt();
    }
  }
   
  
 public static void printArray(String[] list, int numberCards) {
    if (list.length == 52) {
      for (int i = 0; i < 52; i++) {
        System.out.print(list[i] + " ");
      }
      System.out.println();
    }
    else {
      for (int i = 0; i < numberCards; i++) {
        System.out.print(list[i] + " ");
      }
      System.out.println();
    }
  }
  
  
  public static String[] shuffle(String[] list){
    Random randGen = new Random(); // this will randomly generate numbers
    System.out.println("Shuffled Cards: "); // printing the category of shuffled cards
    for (int i = 0; i < 52; i++) {
      int random = randGen.nextInt(50) + 1; // declaring variable "random" to generate a random number within the random
      String nothing = list[0]; 
      list[0] = list[random];
      list[random] = nothing;
    }
    return list;
  }
  
  
  
  public static String[] getHand(String[] list, int index, int numberCards) {
    String[] hand = new String[numberCards]; 
    for (int i = 0; i < numberCards; i++) {
      hand[i] = list[index]; 
      index--; // -- because it will go to the next card available within the deck of cards
    }
    System.out.println("Your Hand:"); //priting the category of your hand
    return hand;
  }
}

   