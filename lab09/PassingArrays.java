/// Shelby Taylor
/// CSE 002 Passing Arrays Inside Methods Lab
/// November 15, 2018

public class PassingArrays {
  public static void main(String [] args) {
    int [] array0 = {1, 2, 3, 4, 5, 6, 7, 8}; // assigning the array0 with the 8 integers
    int [] array1 = copy(array0); 
    int [] array2 = copy(array0);

// passes array0 to intverter and prints
    inverter(array0);
    print(array0);
    System.out.println();

//passes array1 to inverter1 and prints it
    inverter1(array1);
    print(array1);
    System.out.println();

// passes aaray2 to inverter1
    inverter1(array2);

// assigns array3 with array 2 and prints
    int [] array3 = new int [array2.length];
    for (int i = 0; i< array2.length; i++){
    array3[i] = array2[i];
  }
    print(array3);
  }


// makes a copy of the int array that passes through the method
public static int [] copy (int [] array) {
  int [] newArray = new int[array.length];
  for ( int i = 0; i < array.length; i++) {
    newArray[i] = array[i];
  }
  return newArray;
}

// reverses the order of the array
public static void inverter ( int [] array) {
  for (int i = 0; i < array.length; i++) {
    array[i] = array[(array.length - 1) - i];
  }
}

public static int[] inverter1 (int[] array) {
  copy(array);
  inverter(array);
  return array;
}

public static void print (int [] array) {
  for (int i = 0; i < array.length; i++) {
    System.out.print(array[i]);
  }
}
}
