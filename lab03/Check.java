/// Shelby Taylor
/// September 13, 2016
/// CSE 02
/// lab03

import java.util.Scanner;

public class Check{ 
  // main method required for every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); // declare an instance of the scanner object
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // prompting the user for the orginial cost of the check
    double checkCost = myScanner.nextDouble(); // 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // prompting the user for the tip percentage that they wish to pay
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; // we want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); // prompting the user for the number of people that went to dinner
    int numPeople = myScanner.nextInt(); 
    double totalCost;
    double costPerPerson;
    int dollars; // whole dollar amount of cost
    int dimes; // for storing digits
    int pennies; // for storing digits
    totalCost = checkCost * ( 1 + tipPercent); // calculating total cost
    costPerPerson = totalCost / numPeople; // calculating cost per person
    dollars = (int)costPerPerson; // get the whole amount, droppping decimal fraction 
    // the % operator returns the remander
    dimes = (int) (costPerPerson * 10) % 10; // get the dime amount
    pennies = (int) (costPerPerson * 100) % 10; // get the penny ammount
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); // printing the amount each person in the group owes
    
    
  }
}