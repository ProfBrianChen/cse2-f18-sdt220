/// Shelby Taylor
/// December 4, 2018
/// CSE 002 HW 10

import java.util.Scanner;
import java.util.Arrays;
import java.util.*;


public class hw10{
  public static void main (String [] args){
    
    Scanner myScanner = new Scanner(System.in);
    int counter = 0; // initiliaze counter
    
    String [][] grid = { // Initialize a grid before moves are made
        {"1", "2", "3"},
        {"4", "5", "6"},
        {"7", "8", "9"}
      };
    
    
    
    printGrid(grid); // Prints the initial board for the players
    
    
    
    while (counter < 9){
      int player = 1;  // playing 1 is making the first move
      while (player == 1){  
         System.out.println("Player 1: Please enter the number where you would like your move to be made: ");
         while (!myScanner.hasNextInt()){ // user makes an error in her input
           System.out.println("Error. Try Again.");
           myScanner.next();
         }     
         int gridPosition = myScanner.nextInt(); 
         String gamePiece = "o";  // stating that player 1 uses the letter o as her symbol

         while (gridPosition < 1 || gridPosition > 9){  // user makes an error and types a number that is out of the possible range
           System.out.println("Error.Try Again.");
           gridPosition = myScanner.nextInt();
         }    
         grid = player1(grid, gridPosition); 
         printGrid(grid); // prints the grid once moves have been made in the game
        
         if (winner(grid, gamePiece)){ // checking who won
           System.out.println("Yay! Congrats! Player 1 you win!!"); // showing that player 1 wins
           System.exit(0);
         } 
         counter++; // increases counter once moves have been made
         
         if (counter == 9){ // maximum number of moves have been made
           break;
         }
         
         player++; // player 2, your turn!
      }
      
      
      
      
      while (player == 2){
         System.out.println("Player 2: Please enter the number where you would like your move to be made: "); // prompting player 2 to make their move
         while (!myScanner.hasNextInt()){ // user makes an error in what she types, not an integer
           System.out.println("Error. Try Again.");
           myScanner.next();
         }    
         int gridPosition = myScanner.nextInt(); 
         String gamePiece = "x";  // stating that player 2 uses the letter x as her symbol
 
         while (gridPosition < 1 || gridPosition > 9){ // user makes an error and the number she inputs if out of the possible range
           System.out.println("Error. Try Again.");
           gridPosition = myScanner.nextInt();
         }
         printGrid(grid); // prints the grid once moves have been made in the game
         grid = player2(grid, gridPosition); 
         
         if (winner(grid, gamePiece)){ // checking who wins
           System.out.println("Yay! Congrats! Player 2 you win!!"); // player 2 wins, woohoo!
           System.exit(0);
         }
         player--; // player 1's turn again
         counter++; //incremements moves once they have been made
      }
    }
    // if there is no winner, because it is a tie
    System.out.println("Oh No! There is no winner, it's a draw!"); // its a draw
    System.exit(0);  
    
  }
  
 
  public static String [][] player1(String [][] grid, int gridPosition){
    Scanner myScanner = new Scanner(System.in);
    String stringGridPosition = Integer.toString(gridPosition);
    
    int temp = 0; // initializes another counter
    while (temp == 0){ 
      for (int row = 0; row < grid.length; row++){
        for (int col = 0; col < grid[row].length; col++){ 
          if (grid[row][col].equals(stringGridPosition)){ 
            grid[row][col] = "O";                 
            temp++;  // incremement new counter  
            break;  // and break out of loop
          }
        }
      }
      
      
      if (temp == 0){
        System.out.println("Error. Try Again."); // prints error because the spot is no longer available. it has been taken during the game
        stringGridPosition = myScanner.next();
      }
    }
    return grid; 
  }
  

  
  
  public static String [][] player2(String [][] grid, int gridPosition){
    Scanner myScanner = new Scanner(System.in);
    String stringGridPosition = Integer.toString(gridPosition);
    
    int temp = 0; 
    while (temp == 0){ 
      for (int row = 0; row < grid.length; row++){
        for (int col = 0; col < grid[row].length; col++){
          if (grid[row][col].equals(stringGridPosition)){ 
            grid[row][col] = "x"; 
            temp++; // incremement counter
            break; //  break out of loop
          }
        }
      }
      
      
      if (temp == 0){
        System.out.println("Error. Try Again."); // prints error because spot was taken
        stringGridPosition = myScanner.next();
      }
    }
    return grid; 
  }
 
  
  
  public static boolean winner(String [][] grid, String gamePiece){
  
    // checking for winner
    for (int i = 0; i < grid.length; i++) { // horizontal win
      if ((grid[i][0].equals(gamePiece)) && (grid[i][1].equals(gamePiece)) && (grid[i][2].equals(gamePiece))) {
      return true;
      }
     }
     for (int j = 0; j < grid.length; j++) { // vertical win
       if ((grid[0][j].equals(gamePiece)) && (grid[1][j].equals(gamePiece)) && (grid[2][j].equals(gamePiece))) {
       return true;
       }
     }
     if ((grid[0][0].equals(gamePiece)) && (grid[1][1].equals(gamePiece)) && (grid[2][2].equals(gamePiece))) { // diagonal win
       return true;
     }
     if ((grid[0][2].equals(gamePiece)) && (grid[1][1].equals(gamePiece)) && (grid[2][0].equals(gamePiece))) { // diagonal win
       return true;
     } 
     else {
       return false;
     }
  }
  
  
    public static void printGrid(String[][] grid){
    for (int row = 0; row < grid.length; row++){
      for (int col = 0; col < grid[row].length; col++){
        System.out.print(grid[row][col] + " ");
      }
      System.out.println();
    }
  }
}

  