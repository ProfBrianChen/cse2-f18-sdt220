/// Shelby Taylor
/// CSE 002 Lab 08
/// November 8, 2018



import java.util.Arrays;
import java.util.Random;


public class ArraysLab {
  public static void main(String [] args) {

    int [] array1 = new int[100]; 
    int [] array2 = new int[100];
    
    Random rand = new Random();
    for (int i = 0; i < array1.length; i++) {
      int n = rand.nextInt(100);
      array1[i] = n;
    }
    
    System.out.println("array 1 holds the following integers: " + Arrays.toString(array1));
    
    int j = 0;
    int i = 0;
    
    for (i = 0; i < array1.length; i++) {
      j = array1[i];
      array2[j]++;
    }
    
    for (i = 1; i < array2.length; i++) {
      if (array2[i] == 1) {
        System.out.println( i + " occurs " + array2[i] + " time ");
      }
      else if (array2[i] >= 2 || array2[i] == 0) { 
        System.out.println(i + " occurs " + array2[i] + " times "); 
      }    
    }  
  } 
}
    