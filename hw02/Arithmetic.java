/// Shelby Taylor
/// September 6, 2018
/// CSE 02 Arithmetic Calculations
///

public class Arithmetic {
  
  public static void main(String args []){
    
    // my input variables
    int numPants = 3; // Number of pairs of pants
    double pantsPrice = 34.98; // Cost per pair of pants
    int numShirts = 2; // Number of sweatshirts
    double shirtPrice = 24.99; // Cost per shirt
    int numBelts = 1; // number of belts
    double beltCost = 33.99; // cost per belt
    double paSalesTax = 0.06; // the tax rate
    
    // my output variables
    double totalCostOfPants; // total cost of pants
    double totalCostOfShirts; // total cost of shirts
    double totalCostOfBelts; // total cost of belts
    double totalSalesTaxOnPants; // sales tax charged on pants
    double totalSalesTaxOnShirts; // sales tax charged on shirts 
    double totalSalesTaxOnBelts; // sales tax charged on belts
    double totalCostOfPurchases; // total cost of purchases before sales tax 
    double totalSalesTax; // total sales tax 
    double totalAmountPaidWithTax; // total amount paid for the transaction, including sales tax
    
    // calculations for total costs
    totalCostOfPants = numPants * pantsPrice; // calculating the total cost of pants
    totalSalesTaxOnPants = totalCostOfPants * paSalesTax; // calculating the sales tax being charged on the purchase of pants
    totalCostOfShirts = numShirts * shirtPrice; // calculating the total cost of shirts 
    totalSalesTaxOnShirts = totalCostOfShirts * paSalesTax; // calculating the sales tax being charged on the purchase of shirts
    totalCostOfBelts = numBelts * beltCost; // calculating the total cost of belts
    totalSalesTaxOnBelts = totalCostOfBelts * paSalesTax; // calculating the sales tax being charged on the purchase of belts
    totalCostOfPurchases = totalCostOfBelts + totalCostOfShirts + totalCostOfPants; // calculating the total cost of purchases before sales tax 
    totalSalesTax = totalSalesTaxOnBelts + totalSalesTaxOnShirts + totalSalesTaxOnPants; // calculating the total sales tax for the purchase 
    totalAmountPaidWithTax = totalCostOfPurchases + totalSalesTax; // calculating total amount paid for the purchase including tax 
    
    // calculations to ensure nice decimals 
    totalSalesTaxOnPants = totalSalesTaxOnPants * 100; // multiplying total sales tax on pants by 100 in order to begin the process to get 2 decimal points
    totalSalesTaxOnPants = (int)totalSalesTaxOnPants; // changing it from a double to an int 
    totalSalesTaxOnPants = totalSalesTaxOnPants / 100; // changing sales tax to have two decimal points 
    totalSalesTaxOnShirts = totalSalesTaxOnShirts * 100; // multiplying total sales tax on shirts by 100 in order to begin the process to get 2 decimal points 
    totalSalesTaxOnShirts = (int)totalSalesTaxOnShirts; // changing it from a double to an int
    totalSalesTaxOnShirts = totalSalesTaxOnShirts / 100; // changing sales tax to have two decimal places 
    totalSalesTaxOnBelts = totalSalesTaxOnBelts * 100; // multiplying total sales tax on belts by 100 in order to begin the process to get 2 decimal places
    totalSalesTaxOnBelts = (int)totalSalesTaxOnBelts; // changing it from a double to an int
    totalSalesTaxOnBelts = totalSalesTaxOnBelts / 100; // changing sales tax to have two decimal points
    totalSalesTax = totalSalesTax * 100; // multiplying total sales tax by 100 to start the process to ensure correct decimal places for dollar amounts
    totalSalesTax = (int)totalSalesTax; // changing it from a double to an int
    totalSalesTax = totalSalesTax / 100; // finishing the process so that way the value will have two decimal points
    totalAmountPaidWithTax = totalAmountPaidWithTax * 100; // multiplying by 100 to start the process to ensure correct decimal places for dollar amounts
    totalAmountPaidWithTax = (int)totalAmountPaidWithTax; // changing it from a double to an int
    totalAmountPaidWithTax = totalAmountPaidWithTax / 100; // finishing the process so that way the value will have two decimal points
    
    
    // print the output data
    System.out.println("The total cost of pants before tax is $"+totalCostOfPants+"");
    System.out.println("The total cost of shirts before tax is $"+totalCostOfShirts+"");
    System.out.println("The total cost of belts before tax is $"+totalCostOfBelts+"");
    
    System.out.println("The total amount of sales tax on pants is $"+totalSalesTaxOnPants+"");
    System.out.println("The total amount of sales tax on shirts is $"+totalSalesTaxOnShirts+"");
    System.out.println("The total amount of sales tax on belts is $"+totalSalesTaxOnBelts+"");
    
    System.out.println("The total cost of the purchase before tax is $"+totalCostOfPurchases+"");
 
    System.out.println("The total amount of sales tax on the purchase is $"+totalSalesTax+"");
    
    System.out.println("The total amount paid for the purchase, including sales tax, is $"+totalAmountPaidWithTax+"");
    
    
    
  }
}
