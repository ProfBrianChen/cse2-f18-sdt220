/// Shelby Taylor
/// CSE 002 Lab 06
/// October 11. 2018

import java.util.Scanner;
import java.lang.Math;

public class PatternD {
  public static void main (String [] arg){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("Enter a number between 1 and 10:  "); // prompting the user to enter a number between 1 and 10

    int numberInput = myScanner.nextInt(); // the number the user enters above
    
    while ( numberInput > 10 || numberInput < 1) {
      System.out.print("Error.  Try again");
      numberInput = myScanner.nextInt();
    }
    for (int numRows = 1; numRows <= numberInput; numRows++) {
      for (int columnNumber = numberInput; columnNumber >= numRows; columnNumber--) {
      System.out.print(columnNumber + " "); 
      }
      System.out.println("");
    }
    
  }
}