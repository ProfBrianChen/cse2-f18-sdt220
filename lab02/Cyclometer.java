/// Shelby Taylor
/// September 6, 2018
/// CSE 02 Cyclometer
///

public class Cyclometer {
  // main method required for every java program
  public static void main(String [] args) {
    
    // my input data
    int secsTrip1 = 480; // Trip 1 takes 480 seconds
    int secsTrip2 = 3220; // Trip 2 takes 3220 seconds 
    int countsTrip1 = 1561; // Trip 1 has 1561 counts or rotations
    int countsTrip2 = 9037; // Trip 2 has 9037 counts or rotations
    
    // my intermediate variabes and output data
    double wheelDiameter = 27.0, // the wheel diameter is 27.0 
    PI = 3.14159, // the mathematical value for pi
    feetPerMile = 5280, // there are 5280 feet in a mile
    inchesPerFoot = 12, // there are 12 inches in a foot
    secondsPerMinute = 60; // there are 60 seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; // variables for various distances in problem
    
    // print statements above
    System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); 
    
    // running the calculations 
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // caluclates the distance in inches
    
    // for each count, a rotation of the wheel travels the diameter in inches times PI
    
    distanceTrip1 /= inchesPerFoot * feetPerMile; // give s distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI /inchesPerFoot / feetPerMile; 
    totalDistance = distanceTrip1 + distanceTrip2;
    
    // print out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    
    
    
    
  }
}