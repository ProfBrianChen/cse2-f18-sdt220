/// Shelby Taylor
/// CSE 02 Lab 04 CardGenerator.java
/// September 20, 2018

import java.lang.Math;
public class CardGenerator{
  // main method required for every java program
  public static void main(String[] args){
    int cardSuit;
   
    int cardNumber = (int)((Math.random() * 51)) + 1; // random card generator of the 52 cards in the deck
                           
                           if (cardNumber >= 1 && cardNumber <= 13) { // card numbers 1 to 13 will be in the suit of diamonds
                              if (cardNumber == 11) { 
                            System.out.println("You picked the Jack of Diamonds."); // card number 11 is the jack of diamonds
                              }
                              else if (cardNumber == 12) { 
                            System.out.println("You picked the Queen of Diamonds."); // the card number 12 is the queen of diamonds
                               }
                               else if (cardNumber == 13) {
                            System.out.println("You picked the King of Diamonds.");  // card number 13 is the king of diamonds
                              }
                              else if (cardNumber == 1) { 
                          System.out.println("You picked the Ace of Diamonds."); // card number 1 is the ace of diamonds
                             }
                           else System.out.println("You picked the " + cardNumber + " of Diamonds."); // if the card is not a jack, queen, king or ace, it will print the specific number here  
                           }
    
    
                           if (cardNumber >= 14 && cardNumber <= 26) { // card numbers from 14 to 26 will be in the suit of Clubs
                              if (cardNumber == 24) { 
                            System.out.println("You picked the Jack of Clubs."); // card number 24 is the jack of clubs
                             }
                               else if (cardNumber == 25){ 
                            System.out.println("You picked the Queen of Clubs."); // the card number 25 is the queen of clubs
                             }
                              else if (cardNumber == 26) { 
                          System.out.println("You picked the King of Clubs."); // card number 26 is the king of clubs
                             }
                               else if (cardNumber == 14) { 
                              System.out.println("You picked the Ace of Clubs."); // card number 14 is the ace of clubs
                             }
                            else  System.out.println("You picked the " + cardNumber % 13 + " of Clubs."); // if the card is not a jack, queen, king or ace, it will print the specific number here 
                             }
      
    
                           if (cardNumber >= 27 && cardNumber <= 39) { // card numbers 17 to 39 will be in the suit of Hearts
                              if (cardNumber == 37) { 
                            System.out.println("You picked the Jack of Hearts."); // the card number 27 is the jack of hearts
                             }
                              else if (cardNumber == 38) { 
                            System.out.println("You picked the Queen of Hearts."); // card number 38 is the queen of hearts
                             }
                              else if (cardNumber == 39) { 
                          System.out.println("You picked the King of Hearts."); // card number 39 is the king of hearts
                            }
                              else if (cardNumber == 27) { 
                              System.out.println("You picked the Ace of Hearts."); // the card number 27 is the ace of hearts
                             }
                             else System.out.println("You picked the " + cardNumber % 13+ " of Hearts."); // if the card is not a jack, queen, king or ace, it will print the specific number here 
                             }
    
    
    
                           if (cardNumber >= 40 && cardNumber <= 52){ // card numbers 40 to 52 will be in the suit of Spades
                             
                                if (cardNumber == 50) { 
                          System.out.println("You picked the Jack of Spades."); // the card numberber 50 is the jack of spades
                               }
                              else if (cardNumber == 51) { 
                          System.out.println("You picked the Queen of Spades."); // card number 51 is the queen of spades
                               }
                              else if (cardNumber == 52) { 
                          System.out.println("You picked the King of Spades."); // card number 52 is the king of spades
                               }
                              else if (cardNumber == 40) { 
                              System.out.println("You picked the Ace of Sapdes."); // card number 40 is the ace of spades
                               }
                                 else System.out.println("You picked the " + cardNumber % 13 + " of Spades."); // if the card is not a jack, queen, king or ace, it will print the specific number here 
                           }
 
  }
}