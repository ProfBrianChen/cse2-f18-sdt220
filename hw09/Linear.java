/// Shelby Taylor
/// CSE 002 Linear
/// Homework 9
/// November 27, 2018


import java.util.Scanner;
import java.util.Arrays;


public class Linear {
  public static void main (String [] args) {
    System.out.println("Please enter 15 integers: "); // prompts the user to enter 15 integers
    Scanner input = new Scanner(System.in); 
    int counter = 0; // initializing counter
    int[] array1 = new int[15]; 
    
    while (counter < 15) { // as long as the counter is less than 15, go through this loop
      if (input.hasNextInt()) { // ensuring user entered an integer
        array1[counter] = input.nextInt(); 
        if (array1[counter] < 0 || array1[counter] > 100) { // checks to see if the user input an integer outside of the range
          System.out.println("The integer you entered is outside of the possible range.");
          System.exit(0);
        }
        else {
          if (counter > 0) {
            if (array1[counter] < array1[counter - 1] ) { // checking that the user inputted a number that is greater than the last
              System.out.println("The integer you entered is not greater than the last one." );
              System.exit(0);
            }
          }
          counter++;
        }
      }
        else { 
          System.out.println("The number you entered is not an integer.");
          System.exit(0);
        }
      }
      
      
      System.out.println("Sorted: ");
      System.out.println(Arrays.toString(array1)); // prints the converted array to string statement
      System.out.print("Please enter the grade you wish to search for: "); // prompting the user to enter the grade that she wishes to search for
      
      
      if (input.hasNextInt()) {
        int integer = input.nextInt(); 
        BinarySearch(array1, integer); 
        Scramble(array1);
        System.out.println("Scarmbled: "); // printing Scrambled
        System.out.println(Arrays.toString(array1)); // printing the converting scrambled array to string
        System.out.print("Please enter the grade you wish to search for: "); // prompting the user to enter the grade that she wishes to search for
        if (input.hasNextInt()) {
          LinearSearch(array1, integer);  // given that the user entered an integer, linear search method is called
        }
        else { 
          System.out.println("You did not enter an integer."); // printing that the user did not enter an integer
          System.exit(0);
        }
      }
      else {
        System.out.println("You did not enter a grade that you wish to search for. " ); // printing that the user did not enter a grade that she wishes to search for
        System.exit(0);
      }
    }
  
  
  public static void LinearSearch(int [] array1, int k) {
    for ( int i = 0; i < array1.length; i++ ) {
      if (array1[i] == k) {
                System.out.println(k + " was found in the list in " + (i + 1) + " iterations");
                break;
            }
            else if (array1[i] != k && i == (array1.length - 1)){
                System.out.println(k + " was not found in " + (i + 1) + " iterations");
            }
        }
    }

  
  
  public static int[] Scramble(int[] array1) {
    for ( int i = 0; i < array1.length; i++) {
      int k = (int)(Math.random() * array1.length);
      int temp = array1[i]; // temporarily storing number at i index in array
      while ( k != i ) { // while they are not equal
        array1[i] = array1[k];
        array1[k] = temp;
        break;
      }
    }
    return array1;
  }
  
  
  public static void BinarySearch( int [] array2, int j) {
    int high = array2.length - 1;
    int low = 0;
    int counter = 0;
    while ( low <= high) {
      counter++;
      int middle = ((high + low) / 2 );
      
      if (j < array2[middle]) {
        high = middle - 1;
      }
      
      else if ( j > array2[middle]) {
        low = middle + 1;
      }
      
      else if ( j == array2[middle]) {
        System.out.println( j + " was found in " + counter + " iterations"); // printing statement
        break;
      } 
    }
    
    if (low > high ) {
      System.out.println( j + " was not found in " + counter + " iterations");
    }
  }
  
  
}
