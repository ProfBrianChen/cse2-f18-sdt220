/// Shelby Taylor
/// CSE 002 RemoveElements
/// Homework 9
/// November 27, 2018

import java.util.Scanner;

public class RemoveElements
{
  
  public static void main(String [] arg)
  {
    Scanner myScanner = new Scanner(System.in);
    int num[] = new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
   String answer="";
   do
   {
     System.out.print("Random input 10 ints [0-9]");
     num = randomInput();
     String out = "The original array is:";
     out += listArray(num);
     System.out.println(out);
     
      System.out.print("Please enter the index: ");
      index = myScanner.nextInt();
      
      
      if (index < 0 || index > num.length - 1)//ensuring that the indeix is between 0 and 9
      {
        System.out.println("The number you entered was not within the range (0-9).");// printing that the number entered was not within the proper range 
        System.out.println(out);
      }
      
      else 
      {
      newArray1 = delete(num,index);
     String out1="The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      }
   
      
      System.out.print("Please enter the target value: "); // prompting the user the enter the target value
     target = myScanner.nextInt();
     newArray2 = remove(num,target);
     String out2 = "The output array is ";
     out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
     System.out.println(out2);
          
     System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
     answer=myScanner.next();
   }
   
   
      while(answer.equals("Y") || answer.equals("y"));
  }
   
  
  
  public static String listArray(int num[])
  {
    String out = "{";
    for(int j = 0;j < num.length; j++)
    {
     if(j > 0)
     {
        out += ", ";
     }
     out += num[j];
    }
    out += "} ";
    return out;
  }
  
  public static int[] randomInput()
  {
    int num[] = new int[10];// new array of length 10
    for (int i = 0; i < num.length; i++)
    {
      int x = (int)(Math.random()*10); // randomizer
      num[i] = x;
    }
    return num;
  }
  
  
  
  public static int[] delete(int[]array, int y) // delete Method
  {
    int [] array2 = new int[9];// make a new array of length 9 
    
    for (int j = 0; j < y; j++)
    {
      array2[j] = array[j];
    }
    
    for (int i = y + 1; i < array.length; i++)
    {
      array2[y] = array[i];
      y++;
    }
    
    return array2;
  }
  
  
  
  
  public static int[] remove(int[]array, int y)// remove Method
  {
    int counter = 0; // initialize counter
    for (int i = 0; i < array.length; i++) // helping to determine length of new array
    {
      if (array[i] != y)
      {
        counter++;
      }
    }
    
    int array2[] = new int [counter];// creates a new array
    int counter2 = 0;// initializing second counter 
    for (int j = 0; j < array.length; j++)
    {
      if (array[j] != y)
      {
        array2[counter2] = array[j];
        counter2++;
      }
    }
    return array2;
  }
}

    