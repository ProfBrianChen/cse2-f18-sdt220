/// Shelby Taylor
/// CSE 002 lab07
/// October 25, 2018

import java.util.Scanner;
import java.lang.Math;
import java.util.Random;


public class Methods {
  public static void main (String [] arg){    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("The " + Adjective() + " " + Adjective() + " " + Subject1() + " " + Verb() + " the " + Adjective() + " " + Subject2() + ".");
    System.out.println("Do you want another sentence?(type 1 for yes, type 2 for no)");
    int input = myScanner.nextInt();
    if (input == 1) {
      System.out.println("This " + Subject1() + " was " + Adverb() + " "  + Adjective() + " to " + Verb2() + " " + Noun() + ".");
      System.out.println(Pronoun() + " used " + Noun() + " to " + Verb3() + " " + Noun() + " at the " + Adjective() + " " + Subject2() + "." );
      System.out.println("That " + Subject1() + " " + Verb() + " her " + Noun() + "!");
    }
    
    
  }
  public static String Adjective() {
    int adjective = (int)(Math.random()*10);
    switch (adjective) {
      case 0:
        return "funny";
      case 1:
        return "cute";
      case 2:
        return "fast";
      case 3:
        return "tall";
      case 4:
        return "smart";
      case 5:
        return "courageous";
      case 6:
        return "shy";
      case 7: 
        return "determined";
      case 8:
        return "skinny";
      case 9:
        return "athletic";
    }
    return "";
  }
  
  public static String Subject1() {
    int subject1 = (int)(Math.random()*10);
    switch (subject1) {
      case 0:
        return "boy";
      case 1:
        return "Mom";
      case 2:
        return "tiger";
      case 3:
        return "monster";
      case 4:
        return "brother";
      case 5:
        return "dragon";
      case 6:
        return "sister";
      case 7: 
        return "frog";
      case 8:
        return "monkey";
      case 9:
        return "Father";
    }
    return "";
  }
  
  public static String Subject2() {
    int subject2 = (int)(Math.random()*10);
    switch (subject2) {
      case 0:
        return "girl";
      case 1:
        return "Granpda";
      case 2:
        return "Grandma";
      case 3:
        return "rhino";
      case 4:
        return "step-brother";
      case 5:
        return "step-sister";
      case 6:
        return "fox";
      case 7: 
        return "elephant";
      case 8:
        return "cheetah";
      case 9:
        return "puppy";
    }
    return "";
  }
    
  
  public static String Verb() {
    int verb = (int)(Math.random()*10);
    switch (verb) {
      case 0:
        return "ran";
      case 1:
        return "swam";
      case 2:
        return "jumped";
      case 3:
        return "hiked";
      case 4:
        return "passed";
      case 5:
        return "kicked";
      case 6:
        return "walked";
      case 7: 
        return "jogged";
      case 8:
        return "leaped";
      case 9:
        return "helped";
    }
    return "";
  }
  
  public static String Pronoun() {
    int pronoun = (int)(Math.random()*10);
    switch (pronoun) {
      case 0:
        return "it";
      case 1:
        return "they";
      case 2:
        return "you";
      case 3:
        return "she";
      case 4:
        return "he";
      case 5:
        return "we";
      case 6:
        return "I";
      case 7: 
        return "Shelby";
      case 8:
        return "y'all";
      case 9:
        return "them";
    }
    return "";
  }
  
  public static String Noun() {
  int noun = (int)(Math.random()*10);
    switch (noun) {
      case 0:
        return "pencils";
      case 1:
        return "animals";
      case 2:
        return "people";
      case 3:
        return "computers";
      case 4:
        return "girls";
      case 5:
        return "water bottles";
      case 6:
        return "hair ties";
      case 7: 
        return "rings";
      case 8:
        return "calculators";
      case 9:
        return "vacations";
    }
    return "";
  }  
  
    
  public static String Adverb() {
  int adverb = (int)(Math.random()*10);
    switch (adverb) {
      case 0:
        return "particularly";
      case 1:
        return "firmly";
      case 2:
        return "lightly";
      case 3:
        return "beautifully";
      case 4:
        return "truthfully";
      case 5:
        return "quickly";
      case 6:
        return "cheerfully";
      case 7: 
        return "briskly";
      case 8:
        return "weirdly";
      case 9:
        return "wholeheartedly";
    }
    return "";
  }  
  
    
  public static String Verb2() {
  int verb2 = (int)(Math.random()*10);
    switch (verb2) {
      case 0:
        return "passing";
      case 1:
        return "swimming";
      case 2:
        return "walking";
      case 3:
        return "loving";
      case 4:
        return "leaping";
      case 5:
        return "jumping";
      case 6:
        return "singing";
      case 7: 
        return "sleeping";
      case 8:
        return "napping";
      case 9:
        return "talking";
    }
    return "";
  }  
  
    
  public static String Verb3() {
  int verb3 = (int)(Math.random()*10);
    switch (verb3) {
      case 0:
        return "swim";
      case 1:
        return "run";
      case 2:
        return "walk";
      case 3:
        return "leap";
      case 4:
        return "sleep";
      case 5:
        return "nap";
      case 6:
        return "throw";
      case 7: 
        return "jump";
      case 8:
        return "talk";
      case 9:
        return "teach";
    }
    return "";
  }  
  
 
  
}