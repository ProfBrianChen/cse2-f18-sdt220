///Shelby Taylor
///CSE 02 Welcome Class 
/// September 4, 2018
///

public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints    -----------
    ///prints   |  WELCOME  |
    ///prints    -----------
    ///prints   ^  ^  ^  ^  ^  ^ 
    ///prints  / \/ \/ \/ \/ \/ \
    ///prints <-S--D--T--2--2--0->
    ///prints  \ /\ /\ /\ /\ /\ /
    ///prints   v  v  v  v  v  v
    
    System.out.println("    ----------- ");
    System.out.println("   |  WELCOME  |");
    System.out.println("    ----------- ");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-S--D--T--2--2--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
                       
    ///prints tweet-length autobiographic statement
    System.out.println("My name is Shelby Taylor and I am a junior ISE major at Lehigh University");
      
  }
}