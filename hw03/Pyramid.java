/// Shelby Taylor
/// hw03 Pyramid.java
/// September 14, 2018
/// CSE 02

import java.util.Scanner;

public class Pyramid{
  // main method required for every java program
  public static void main (String[] args){
    Scanner myScanner = new Scanner ( System.in ); // declare an instance of the scanner object
    System.out.print("The square side of the pyramid is (input length): "); // prompting the user to input the square side length of the pyramid
    int sqSide = myScanner.nextInt();
    System.out.print("The height of the pyramid is (input height):"); // prompting the user to input the height of the pyramid 
    int height = myScanner.nextInt();
    int totalVolume; // declaring variable for calculating the volume inside the pyramid
    
    // begin calculations
    totalVolume = ((sqSide * sqSide * height) / 3); // calculating the volume inside the pyramid
    
    System.out.println("The volyme inside the pyramid is: " + totalVolume); // printing the volyme inside the pyramid 
    
    
  }
}