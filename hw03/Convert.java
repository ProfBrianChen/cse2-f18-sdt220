/// Shelby Taylor
/// hw03 Convert.java
/// Septemeber 14, 2018
/// CSE 02

import java.util.Scanner;

public class Convert{
  // main method required for every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); // declare an instance of the scanner object
    System.out.print("Enter the affected area in acres: "); // prompting the user to enter the affected area in acres
    double areaInAcres = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area: "); // prompting the user to enter the amount of rainfall in the affected area in inches
    int rainfall = myScanner.nextInt();
    
    int numGallonsPerAcreInch; // the number of acre inch in a gallon for conversion purposes
    double numCubicMilesInGallon; // the number of cubic miles in a gallon for conversion purposes
    double quantityOfRain; 
    // declare variables
    
    numGallonsPerAcreInch = 27154; // the number of gallons in an acre inch
    numCubicMilesInGallon = 9.08169E-13; // the number of cubic miles in a gallon
    
    // begin calculations to find the quantity of rain in cubic miles
    
    quantityOfRain = areaInAcres * rainfall;
    quantityOfRain = quantityOfRain * numGallonsPerAcreInch;
    quantityOfRain = quantityOfRain * numCubicMilesInGallon;
    System.out.println(+ quantityOfRain + " cubic miles"); // print the quantity of rain in cubic miles  
    
  }
}