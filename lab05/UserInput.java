/// Shelby Taylor
/// October 4, 2018
/// CSE 02 Lab05 User Input

import java.util.Scanner;

public class UserInput { // main method required
  public static void main (String [] args){
    Scanner myScanner = new Scanner(System.in);
    
  System.out.print("Enter your course number:  "); // enter the number of your course
    boolean correct = myScanner.hasNextInt();
    
    while (correct == false) {
      System.out.print("Wrong input. Try again.");
      String junk = myScanner.next();
      System.out.print("Enter your course number:  ");
      correct = myScanner.hasNextInt();
    }
    
    if (correct) {
      int courseNumber = myScanner.nextInt();
    }
    
    
    System.out.print("Enter your department:  "); // enter the name of your department (ex: Math)
      boolean nameOfDepartment = myScanner.hasNextInt();
        
        while (nameOfDepartment == true) {
          System.out.print("Wrong input. Try again.");
          String junk = myScanner.next();
          System.out.print("Enter your department:  ");
          nameOfDepartment = myScanner.hasNextInt();
        }
    if (nameOfDepartment == false) {
      String department = myScanner.next();
    }
    
    System.out.print("Enter the number of times it meets per week:  "); // enter the number of times your class meets in a week
      boolean meetingsPerWeek = myScanner.hasNextInt();
        
        while (meetingsPerWeek == false) {
          System.out.print("Wrong input.  Try again.");
          String junk = myScanner.next();
          System.out.print("Enter the number of times it meets per week:  ");
          meetingsPerWeek = myScanner.hasNextInt();
        }
    if (meetingsPerWeek) {
      int numberOfMeetings = myScanner.nextInt();
    }
  
    
    System.out.print("Enter the start time for you class in military time (ex 12:05 pm = 1205):  "); // enter the start time your class, in militay time to make it easier
      boolean classTime = myScanner.hasNextDouble();
    
    while (classTime == false){
      System.out.print("Wrong input. Try again.");
      String junk = myScanner.next();
      System.out.print("Enter the start time for you class in military time:  ");
      classTime = myScanner.hasNextDouble();
    }
    if (classTime) {
      double startTime = myScanner.nextDouble();
    }
    
    System.out.print("Enter the professor's name:  "); // enter your professor's name
    boolean professor = myScanner.hasNextInt();
    
    while(professor == true){
      System.out.print("Wrong input. Try again.");
      String junk = myScanner.next();
      System.out.print("Enter your professor's name:  ");
      professor = myScanner.hasNextInt();
    }
    if (professor == false){
      String nameOfProfessor = myScanner.next();
    }
    
    System.out.print("Enter the number of students in the class:  "); // enter the number of students in your class
      boolean students = myScanner.hasNextInt();
    
    while (students == false){
      System.out.print("Wrong input. Try again.");
      String junk = myScanner.next();
      System.out.print("Enter the number of students in the class:  ");
      students = myScanner.hasNextInt();
    }
    if (students){
      int numberOfStudents = myScanner.nextInt();
    }
    
}
}