/// Shelby Taylor
/// CSE 02 HW 04 CrapsIf.java
/// September 20, 2018

import java.lang.Math; 
import java.util.Scanner;

public class CrapsIf { 
// main method required for every java program
public static void main (String [] args){ 
Scanner myScanner = new Scanner( System.in ); // declare an instance of the scanner object
System.out.print("If you would like to randomly cast two dice, type '1'.  If you would like to state the two dice you would like to evaluate, type '2':  "); // prompting the user to decide about the game of craps
int decision = myScanner.nextInt();

int diceOne;
int diceTwo;
  
if (decision == 2) {
System.out.print("Type the first value for dice number 1 (from 1 to 6, inclusive): "); // prompting the user to type the first value for dice number 1, between 1 and 6
diceOne = myScanner.nextInt();
System.out.print("Type the second value for dice number 2 (from 1 to 6 inclusive): "); // prompting the user to type the second vale for dice number 2, between 1 and 6
diceTwo = myScanner.nextInt();
  
  if (diceOne == 1 && diceTwo == 1) {
System.out.println("The slang terminology for this roll is Snake Eyes"); // rolling 2 ones produces the slang term snake eyes in the game of craps
}

else if ((diceOne == 1 && diceTwo == 2) || diceOne == 2 && diceTwo == 1) {
System.out.println("The slang terminology for this roll is Ace Duece"); // if one dice rolls a 1, and the other dice rolls a 2, then the slang term for that roll is Ace Duece
}

else if (diceOne == 2 && diceTwo == 2) {
System.out.println("The slang terminology for this roll is Hard Four");  // rolling 2 twos has a slang term of Hard Four in the game of craps
}

else if ((diceOne == 1 && diceTwo == 3) || (diceOne == 3 && diceTwo == 1)) {
System.out.println("The slang terminology for this role is Easy Four"); // if one dice rolls a 1, and the other dice rolls a 3, then the slang term for that roll is Easy Four
}

else if ((diceOne == 2 && diceTwo == 3) || (diceOne == 3 && diceTwo == 2)) {
System.out.println("The slang terminology for this role is Fever Five"); // if one dice rolls a 2 and the other dice rolls a 3, then the slang term for that roll is Fever Five
}

else if (diceOne == 3 && diceTwo == 3) {
System.out.println("The slang terminology for this roll is Hard Six");  // rolling 2 threes has a slang term of Hard Six in the game of craps
}

else if ((diceOne == 1 && diceTwo == 4) || (diceOne == 4 && diceTwo == 1)) {
System.out.println("The slang terminology for this roll is Fever Five"); // if one dice rolls a 1 and the other dice rolls a 4, then the slang term for this roll is Fever Five
}

else if ((diceOne == 2 && diceTwo == 4) || (diceOne == 4 && diceTwo == 2)) {
System.out.println("The slang terminology for this roll is Easy Six"); // if one dice rolls a 2 and the other dice rolls a 4, then the slang term for this roll is easy six
}

else if ((diceOne == 3 && diceTwo == 4) || (diceOne == 4 && diceTwo == 3 )) {
System.out.println("The slang terminology for this roll is Seven Out");  // if one dice rolls a 3 and the other dice rolls a 4, then the slang term for this roll is Seven Out
}

else if (diceOne == 4 && diceTwo == 4) {
System.out.println("The slang terminology for this roll is Hard Eight"); // rolling 2 fours has a slang term of hard eight in a game of craps
}

else if ((diceOne == 1 && diceTwo == 5) || (diceOne == 5 && diceTwo == 1 )) {
System.out.println("The slang terminology for this roll is Easy Six");  // if one dice rolls a 1 and the other dice rolls a 5, then the slang term for this roll is Easy Six
}

else if ((diceOne == 2 && diceTwo == 5) || (diceOne == 5 && diceTwo == 2 )) {
System.out.println("The slang terminology for this roll is Seven Out");  // if one dice rolls a 2 and the other dice rolls a 5, then the slang term for this roll is Seven Out
}

else if ((diceOne == 3 && diceTwo == 5) || (diceOne == 5 && diceTwo == 3 )) {
System.out.println("The slang terminology for this roll is Easy Eight");  // if one dice rolls a 3 and the other dice rolls a 5, then the slang term for this roll is Easy Eight
}

else if ((diceOne == 4 && diceTwo == 5) || (diceOne == 5 && diceTwo == 4)) {
System.out.println("The slang terminology for this roll is Nine");  // if one dice rolls a 4 and the other dice rolls a 5, then the slang term for this roll is Nine
}

else if (diceOne == 5 && diceTwo == 5) {
System.out.println("The slang terminology for this roll is Hard Ten");  // rolling 2 fives has a slang term of hard ten in a game of craps
}

else if ((diceOne == 1 && diceTwo == 6) || (diceOne == 6 && diceTwo == 1 )) {
System.out.println("The slang terminology for this roll is Seven Out");  // if one dice rolls a 1 and the other dice rolls a 6, then the slang term for this roll is Seven Out
}

else if ((diceOne == 2 && diceTwo == 6) || (diceOne == 6 && diceTwo == 2 )) {
System.out.println("The slang terminology for this roll is Easy Eight");  // if one dice rolls a 2 and the other dice rolls a 6, then the slang term for this roll is Easy Eight
}

else if ((diceOne == 3 && diceTwo == 6) || (diceOne == 6 && diceTwo == 3 )) {
System.out.println("The slang terminology for this roll is Nine");  // if one dice rolls a 3 and the other dice rolls a 6, then the slang term for this roll is Nine
}

else if ((diceOne == 6 && diceTwo == 4) || (diceOne == 4 && diceTwo == 6 )) {
System.out.println("The slang terminology for this roll is Easy Ten");  // if one dice rolls a 6 and the other dice rolls a 4, then the slang term for this roll is Easy Ten
}

else if ((diceOne == 6 && diceTwo == 5) || (diceOne == 5 && diceTwo == 6 )) {
System.out.println("The slang terminology for this roll is Yo-leven");  // if one dice rolls a 5 and the other dice rolls a 6, then the slang term for this roll is Yo-leven
}

else if (diceOne == 6 && diceTwo == 6) {
System.out.println("The slang terminology for this roll is Boxcars");  // rolling 2 sixes has a slang term of boxcars in a game of craps
}
  else if (diceOne < 1 || diceOne > 6 || diceTwo < 1 || diceTwo > 6) {
    System.out.println("These numbers are invalid"); // rolling a number less than one or greater than 6 is not possible 
  }
}
  
else if (decision == 1) {
diceOne = (int)((Math.random() * 5)) + 1; // random generator for the first dice with faces between 1 and 6
diceTwo = (int)((Math.random() * 5)) + 1; // random generator for the second dice with faces between 1 and 6


if (diceOne == 1 && diceTwo == 1) {
System.out.println("The slang terminology for this roll is Snake Eyes"); // rolling 2 ones produces the slang term snake eyes in the game of craps
}

else if ((diceOne == 1 && diceTwo == 2) || diceOne == 2 && diceTwo == 1) {
System.out.println("The slang terminology for this roll is Ace Duece"); // if one dice rolls a 1, and the other dice rolls a 2, then the slang term for that roll is Ace Duece
}

else if (diceOne == 2 && diceTwo == 2) {
System.out.println("The slang terminology for this roll is Hard Four");  // rolling 2 twos has a slang term of Hard Four in the game of craps
}

else if ((diceOne == 1 && diceTwo == 3) || (diceOne == 3 && diceTwo == 1)) {
System.out.println("The slang terminology for this roll is Easy Four"); // if one dice rolls a 1, and the other dice rolls a 3, then the slang term for that roll is Easy Four
}

else if ((diceOne == 2 && diceTwo == 3) || (diceOne == 3 && diceTwo == 2)) {
System.out.println("The slang terminology for this roll is Fever Five"); // if one dice rolls a 2 and the other dice rolls a 3, then the slang term for that roll is Fever Five
}

else if (diceOne == 3 && diceTwo == 3) {
System.out.println("The slang terminology for this roll is Hard Six");  // rolling 2 threes has a slang term of Hard Six in the game of craps
}

else if ((diceOne == 1 && diceTwo == 4) || (diceOne == 4 && diceTwo == 1)) {
System.out.println("The slang terminology for this roll is Fever Five"); // if one dice rolls a 1 and the other dice rolls a 4, then the slang term for this roll is Fever Five
}

else if ((diceOne == 2 && diceTwo == 4) || (diceOne == 4 && diceTwo == 2)) {
System.out.println("The slang terminology for this roll is Easy Six"); // if one dice rolls a 2 and the other dice rolls a 4, then the slang term for this roll is easy six
}

else if ((diceOne == 3 && diceTwo == 4) || (diceOne == 4 && diceTwo == 3 )) {
System.out.println("The slang terminology for this roll is Seven Out");  // if one dice rolls a 3 and the other dice rolls a 4, then the slang term for this roll is Seven Out
}

else if (diceOne == 4 && diceTwo == 4) {
System.out.println("The slang terminology for this roll is Hard Eight"); // rolling 2 fours has a slang term of hard eight in a game of craps
}

else if ((diceOne == 1 && diceTwo == 5) || (diceOne == 5 && diceTwo == 1 )) {
System.out.println("The slang terminology for this roll is Easy Six");  // if one dice rolls a 1 and the other dice rolls a 5, then the slang term for this roll is Easy Six
}

else if ((diceOne == 2 && diceTwo == 5) || (diceOne == 5 && diceTwo == 2 )) {
System.out.println("The slang terminology for this roll is Seven Out");  // if one dice rolls a 2 and the other dice rolls a 5, then the slang term for this roll is Seven Out
}

else if ((diceOne == 3 && diceTwo == 5) || (diceOne == 5 && diceTwo == 3 )) {
System.out.println("The slang terminology for this roll is Easy Eight");  // if one dice rolls a 3 and the other dice rolls a 5, then the slang term for this roll is Easy Eight
}

else if ((diceOne == 4 && diceTwo == 5) || (diceOne == 5 && diceTwo == 4)) {
System.out.println("The slang terminology for this roll is Nine");  // if one dice rolls a 4 and the other dice rolls a 5, then the slang term for this roll is Nine
}

else if (diceOne == 5 && diceTwo == 5) {
System.out.println("The slang terminology for this roll is Hard Ten");  // rolling 2 fives has a slang term of hard ten in a game of craps
}

else if ((diceOne == 1 && diceTwo == 6) || (diceOne == 6 && diceTwo == 1 )) {
System.out.println("The slang terminology for this roll is Seven Out");  // if one dice rolls a 1 and the other dice rolls a 6, then the slang term for this roll is Seven Out
}

else if ((diceOne == 2 && diceTwo == 6) || (diceOne == 6 && diceTwo == 2 )) {
System.out.println("The slang terminology for this roll is Easy Eight");  // if one dice rolls a 2 and the other dice rolls a 6, then the slang term for this roll is Easy Eight
}

else if ((diceOne == 3 && diceTwo == 6) || (diceOne == 6 && diceTwo == 3 )) {
System.out.println("The slang terminology for this roll is Nine");  // if one dice rolls a 3 and the other dice rolls a 6, then the slang term for this roll is Nine
}

else if ((diceOne == 6 && diceTwo == 4) || (diceOne == 4 && diceTwo == 6 )) {
System.out.println("The slang terminology for this roll is Easy Ten");  // if one dice rolls a 6 and the other dice rolls a 4, then the slang term for this roll is Easy Ten
}

else if ((diceOne == 6 && diceTwo == 5) || (diceOne == 5 && diceTwo == 6 )) {
System.out.println("The slang terminology for this roll is Yo-leven");  // if one dice rolls a 5 and the other dice rolls a 6, then the slang term for this roll is Yo-leven
}

else if (diceOne == 6 && diceTwo == 6) {
System.out.println("The slang terminology for this roll is Boxcars");  // rolling 2 sixes has a slang term of boxcars in a game of craps
}
   else if (diceOne < 1 || diceOne > 6 || diceTwo < 1 || diceTwo > 6) {
    System.out.println("These numbers are invalid"); // rolling a number less than one or greater than 6 is not possible 
  }
}
}
}