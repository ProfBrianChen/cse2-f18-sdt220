/// Shelby Taylor
/// CSE 02 CrapsSwitch
/// September 23, 2018

import java.util.Scanner;  
import java.lang.Math; 
import java.util.Random;

public class CrapsSwitch {
  // main method required for every java program
  public static void main (String [] args){
    Scanner myScanner = new Scanner (System.in);
    
    int diceOne; // declaring the variable for the number that will appear on dice One
    int diceTwo; // declaring the variable for the number that will appear on dice two
    int outcomeDecisionTwo; // declaring the variable for the outcome that will happen depending on whether they decide to do type 1 or type 2
    int outcomeDecisionOne; //declaring the variable for the outcome that will happen depending on whether they decide to do type 1 or type 2
    
    
    System.out.print("If you would like to randomly cast two dice, type '1'.  If you would like to state the two dice you would like to evaluate, type '2':");  // prompting the user to decide how they would like to play the game of craps
    int decision = myScanner.nextInt();
    
    switch (decision) {
      case 1:
      outcomeDecisionOne = (int)((Math.random() * 65)) + 1; // random generator for the first dice with faces between 1 and 6 (based on the way I did my cases, i needed the numbers to be back to back.)

       switch (outcomeDecisionOne) {
       case 11:
        diceOne = 1;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Snake Eyes"); // the slang terminology for a double one roll is snake eyes
        break;
      case 12:
        diceOne = 1;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Ace Duece"); // the slang terminology for rolling a 1 and a 2 is Ace Duece
        break;
      case 21:
        diceOne = 2;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Ace Duece");  // the slang terminology for rolling a 2 and a 1 is Ace Duece
        break;
      case 22: 
        diceOne = 2;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Hard Four"); // the slang terminology for rolling a double two is hard four
        break;
      case 23: 
        diceOne = 2;
        diceTwo = 3;
        System.out.println("The slang terminology for this role is Fever Five"); // the slang terminology for rolling a 2 and a 3 is fever five
        break;
      case 32:
        diceOne = 3;
        diceTwo = 2;
        System.out.println("The slang terminology for this role is Fever Five"); // the slang terminology for rolling a 3 and a 2 is fever five
        break;
      case 13:
        diceOne = 1;
        diceTwo = 3;
        System.out.println("The slang terminology for this role is Easy Four"); // the slang terminology for rolling a 1 and a 3 is easy four
        break;
      case 31:
        diceOne = 3;
        diceTwo = 1;
        System.out.println("The slang terminology for this role is Easy Four"); // the slang terminology for rolling a 3 and a 1 is easy four
        break;
      case 33:
        diceOne = 3;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Hard Six"); // the slang terminology for rolling double 3's is hard six
        break;
      case 14:
        diceOne = 1;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Fever Five");  // the slang terminology for rolling a 1 and a 4 is fever five
        break;
      case 41:
        diceOne = 4;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Fever Five");  // the slang terminology for rolling a 4 and a 1 is fever five
        break;
      case 24:
        diceOne = 2;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Easy Six");  // the slang terminology for rolling a 2 and a 4 is easy six
        break;
      case 42:
        diceOne = 4;
        diceTwo = 2; 
        System.out.println("The slang terminology for this roll is Easy Six");  // the slang terminology for rolling a 4 and a 2 is easy six
        break;
      case 34: 
        diceOne = 3;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 3 and a 4 is seven out
        break;
      case 43:
        diceOne = 4;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 4 and a 3 is seven out
        break;
      case 44:
        diceOne = 4;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Hard Eight"); // the slang terminology for rolling double fours is hard eight
        break;
      case 15:
        diceOne = 1;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Easy Six"); // the slang terminology for rolling a 1 and a 5 is easy six
        break;
      case 51:
        diceOne = 5;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Easy Six"); // the slang terminology for rolling a 5 and a 1 is easy six
        break;
      case 25:
        diceOne = 2;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 2 and a 5 is seven out
        break;
      case 52:
        diceOne = 5;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 5 and a 2 is seven out
        break;
      case 35:
        diceOne = 3;
        diceTwo = 5;
        System.out.println ("The slang terminology for this roll is Easy Eight");  // the slang terminology for rolling a 3 and a 5 is easy eight
        break;
      case 53:
        diceOne =5;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Easy Eight"); // the slang terminology for rolling a 5 and a 3 is easy eight
        break;
      case 45: 
        diceOne = 4;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 4 and a 5 is nine
        break;
      case 54:
        diceOne = 5;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 5 and a 4 is nine
        break;
      case 55: 
        diceOne = 5;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Hard Ten"); // the slang terminology for rolling double fives is hard ten
        break;
      case 16:
        diceOne = 1;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 1 and a 6 is seven out
        break;
      case 61:
        diceOne = 6;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 6 and a 1 is seven out
        break;
      case 26:
        diceOne = 2;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Easy Eight"); // the slang terminology for rolling a 2 and a 6 is easy eight
        break;
      case 62:
        diceOne = 6;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Easy Eight"); // the slang terminology for rolling a 6 and a 2 is easy eight
        break;
      case 36:
        diceOne = 3;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 3 and a 6 is nine
        break;
      case 63:
        diceOne = 6;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 3 and a 6 is nine
        break;
      case 46:
        diceOne = 4;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Easy Ten"); // the slang terminology for rolling a 4 and a 6 is easy ten
        break;
      case 64:
        diceOne = 6;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Easy Ten"); // the slang terminology for rolling a 6 and a 4 is easy ten
        break;
      case 56:
        diceOne = 5;
        diceTwo = 6; 
        System.out.println("The slang terminology for this roll is Yo-leven"); // the slang terminology for rolling a 5 and a 6 is yo-leven
        break;
      case 65:
        diceOne = 6;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Yo-leven"); // the slang terminology for rolling a 6 and a 5 is yo-leven
        break;
      case 66: 
        diceOne = 6;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Boxcars"); // the slang terminology for rolling double sixes is boxcars
        break;
      default:
         System.out.println("Invalid Response from random generator, try again"); // the random number that was generated did not pick a valid case, try again.
        break;
        // if no cases match
            
        }
        
    break;
        
      case 2:
        System.out.print("Type the values of the dice you want in a row with no spaces (example: 22 => first roll will be 2, second roll will be 2): ");
        outcomeDecisionTwo = myScanner.nextInt();
      switch (outcomeDecisionTwo)  {
      case 11:
        diceOne = 1;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Snake Eyes"); // the slang terminology for a double one roll is snake eyes
        break;
      case 12:
        diceOne = 1;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Ace Duece"); // the slang terminology for rolling a 1 and a 2 is Ace Duece
        break;
      case 21:
        diceOne = 2;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Ace Duece");  // the slang terminology for rolling a 2 and a 1 is Ace Duece
        break;
      case 22: 
        diceOne = 2;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Hard Four"); // the slang terminology for rolling a double two is hard four
        break;
      case 23: 
        diceOne = 2;
        diceTwo = 3;
        System.out.println("The slang terminology for this role is Fever Five"); // the slang terminology for rolling a 2 and a 3 is fever five
        break;
      case 32:
        diceOne = 3;
        diceTwo = 2;
        System.out.println("The slang terminology for this role is Fever Five"); // the slang terminology for rolling a 3 and a 2 is fever five
        break;
      case 13:
        diceOne = 1;
        diceTwo = 3;
        System.out.println("The slang terminology for this role is Easy Four"); // the slang terminology for rolling a 1 and a 3 is easy four
        break;
      case 31:
        diceOne = 3;
        diceTwo = 1;
        System.out.println("The slang terminology for this role is Easy Four"); // the slang terminology for rolling a 3 and a 1 is easy four
        break;
      case 33:
        diceOne = 3;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Hard Six"); // the slang terminology for rolling double 3's is hard six
        break;
      case 14:
        diceOne = 1;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Fever Five");  // the slang terminology for rolling a 1 and a 4 is fever five
        break;
      case 41:
        diceOne = 4;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Fever Five");  // the slang terminology for rolling a 4 and a 1 is fever five
        break;
      case 24:
        diceOne = 2;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Easy Six");  // the slang terminology for rolling a 2 and a 4 is easy six
        break;
      case 42:
        diceOne = 4;
        diceTwo = 2; 
        System.out.println("The slang terminology for this roll is Easy Six");  // the slang terminology for rolling a 4 and a 2 is easy six
        break;
      case 34: 
        diceOne = 3;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 3 and a 4 is seven out
        break;
      case 43:
        diceOne = 4;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 4 and a 3 is seven out
        break;
      case 44:
        diceOne = 4;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Hard Eight"); // the slang terminology for rolling double fours is hard eight
        break;
      case 15:
        diceOne = 1;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Easy Six"); // the slang terminology for rolling a 1 and a 5 is easy six
        break;
      case 51:
        diceOne = 5;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Easy Six"); // the slang terminology for rolling a 5 and a 1 is easy six
        break;
      case 25:
        diceOne = 2;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 2 and a 5 is seven out
        break;
      case 52:
        diceOne = 5;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 5 and a 2 is seven out
        break;
      case 35:
        diceOne = 3;
        diceTwo = 5;
        System.out.println ("The slang terminology for this roll is Easy Eight");  // the slang terminology for rolling a 3 and a 5 is easy eight
        break;
      case 53:
        diceOne =5;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Easy Eight"); // the slang terminology for rolling a 5 and a 3 is easy eight
        break;
      case 45: 
        diceOne = 4;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 4 and a 5 is nine
        break;
      case 54:
        diceOne = 5;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 5 and a 4 is nine
        break;
      case 55: 
        diceOne = 5;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Hard Ten"); // the slang terminology for rolling double fives is hard ten
        break;
      case 16:
        diceOne = 1;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 1 and a 6 is seven out
        break;
      case 61:
        diceOne = 6;
        diceTwo = 1;
        System.out.println("The slang terminology for this roll is Seven Out"); // the slang terminology for rolling a 6 and a 1 is seven out
        break;
      case 26:
        diceOne = 2;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Easy Eight"); // the slang terminology for rolling a 2 and a 6 is easy eight
        break;
      case 62:
        diceOne = 6;
        diceTwo = 2;
        System.out.println("The slang terminology for this roll is Easy Eight"); // the slang terminology for rolling a 6 and a 2 is easy eight
        break;
      case 36:
        diceOne = 3;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 3 and a 6 is nine
        break;
      case 63:
        diceOne = 6;
        diceTwo = 3;
        System.out.println("The slang terminology for this roll is Nine"); // the slang terminology for rolling a 3 and a 6 is nine
        break;
      case 46:
        diceOne = 4;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Easy Ten"); // the slang terminology for rolling a 4 and a 6 is easy ten
        break;
      case 64:
        diceOne = 6;
        diceTwo = 4;
        System.out.println("The slang terminology for this roll is Easy Ten"); // the slang terminology for rolling a 6 and a 4 is easy ten
        break;
      case 56:
        diceOne = 5;
        diceTwo = 6; 
        System.out.println("The slang terminology for this roll is Yo-leven"); // the slang terminology for rolling a 5 and a 6 is yo-leven
        break;
      case 65:
        diceOne = 6;
        diceTwo = 5;
        System.out.println("The slang terminology for this roll is Yo-leven"); // the slang terminology for rolling a 6 and a 5 is yo-leven
        break;
      case 66: 
        diceOne = 6;
        diceTwo = 6;
        System.out.println("The slang terminology for this roll is Boxcars"); // the slang terminology for rolling double sixes is boxcars
        break;
      default:
        break;
        // if no cases match
      }
    }
  }
}