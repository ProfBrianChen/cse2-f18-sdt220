/// Shelby Taylor
/// CSE 002 Lab 10
/// December 6, 2018

import java.util.Arrays;

public class SelectionSortLab10 {
  public static void main(String [] args) {
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int [] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    int iterBest = selectionSort(myArrayBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the shorted array: " + iterBest);
    System.out.println("The total number of operatins performed on the reversed sorted array: " + iterWorst);
  }
  
  // the method for sorting the numbers
  public static int selectionSort (int[] list) {
    // Prints the initial array (you must insert another
  // print out statement later in the code to show the 

  System.out.println(Arrays.toString(list));
  
    int iterations = 0; // Initialize counter for iterations
    int temp = 0; // initialize temp variable
    for (int i = 0; i < list.length - 1; i++) {
   
   iterations++; // Update the iterations counter
   
   // Step One: Find the minimum in the list[i..list.length-1]
   int currentMin = list[i]; 
   int currentMinIndex = i;
   for (int j = i + 1; j < list.length; j++) { 
     if (list[j] < list[iterations]) {
       iterations++;
     }
     
    // COMPLETE THIS FOR LOOP
    // (In this step, make sure you update the iteration 
    // counter time you compare the current min to another 
    // element in the array.)
   }

   // Step Two: Swap list[i] with the minimum you found above
   if (currentMinIndex != i) { 
    // COMPLETE THE CODE TO MAKE THE SWAP 
          temp = list[i];
     list[i] = list[iterations];
     list[iterations] = temp;
   }
  System.out.println(iterations);
   
  }

  return iterations;
 }
}
